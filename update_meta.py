import random
import time
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import re
import string
import numpy as np

import pandas as pd
from sklearn.model_selection import train_test_split
import cv2
import os

raw_data = pd.read_csv("/Users/roy/College/NLP/DL-FinalProject git/meta.csv", encoding='utf8', usecols=['age', 'gender', 'path'])
data = {}
for index, img_meta in enumerate(raw_data.values):
    temp = img_meta[2].split('/') 
    res = ('/'.join(temp[:2]), '/'.join(temp[2:]))[1]

    img_meta[2] = res
    data[index] = img_meta

skrt = pd.DataFrame.from_dict(data, orient='index',
                       columns=['age', 'gender', 'path'])
skrt.to_csv(r'/Users/roy/College/NLP/DL-FinalProject git/meta_new.csv', index = False)